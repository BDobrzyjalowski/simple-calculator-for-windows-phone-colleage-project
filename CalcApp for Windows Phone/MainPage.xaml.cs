﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Szablon elementu Pusta strona jest udokumentowany na stronie http://go.microsoft.com/fwlink/?LinkId=391641

namespace CalcApp_for_Windows_Phone
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie, lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Wywoływane, gdy ta strona ma być wyświetlona w ramce.
        /// </summary>
        /// <param name="e">Dane zdarzenia, opisujące, jak została osiągnięta ta strona.
        /// Ten parametr jest zazwyczaj używany do konfigurowania strony.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Tutaj przygotuj stronę do wyświetlenia.

            // TODO: Jeśli aplikacja zawiera wiele stron, upewnij się, że jest
            // obsługiwany przycisk Wstecz sprzętu, rejestrując
            // zdarzenie Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Jeśli używasz obiektu NavigationHelper dostarczanego w niektórych szablonach,
            // to zdarzenie jest już obsługiwane.
        }
        public void PlusOrMinus()
        {
            if (plus_minus_button == true)
            {
                double tmp;
                double.TryParse(textBox.Text, out tmp);
                tmp = tmp * (-1);
                textBox.Text = tmp.ToString();
            }
        }

        String number_one = "";
        String number_two = "";
        bool plus_minus_button = false;
        bool dot_button = false;
        double one = 0;
        double two = 0;
        bool first_number = true;
        int number_of_operation = 0;

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "1";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "1";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "2";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "2";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "3";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "3";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "4";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "4";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "5";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "5";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "6";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "6";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "7";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "7";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button8_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "8";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "8";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button9_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "9";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "9";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void button0_Click(object sender, RoutedEventArgs e)
        {
            if (first_number == true)
            {
                number_one += "0";
                textBox.Text = number_one;
            }
            else
            {
                number_two += "0";
                textBox.Text = number_two;
            }
            PlusOrMinus();
        }

        private void buttonDot_Click(object sender, RoutedEventArgs e)
        {
            if (dot_button == false)
            {
                if (first_number == true)
                {
                    number_one += ",";
                    textBox.Text = number_one;
                    dot_button = true;
                }
                else
                {
                    dot_button = true;
                    number_two += ",";
                    textBox.Text = number_two;
                }
                PlusOrMinus();
            }
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            number_one = "";
            number_two = "";
            dot_button = false;
            textBox.Text = "";
            one = 0;
            two = 0;
            first_number = true;
            plus_minus_button = false;
        }

        private void buttonPlusMinus_Click(object sender, RoutedEventArgs e)
        {
            if (plus_minus_button == false)
            {
                plus_minus_button = true;
            }
            else
            {
                plus_minus_button = false;
            }
            double tmp;
            double.TryParse(textBox.Text, out tmp);
            tmp = tmp * (-1);
            textBox.Text = tmp.ToString();
        }

        private void buttonPlus_Click(object sender, RoutedEventArgs e)
        {
            if (number_two.Equals("") == false)
            {
                double result = 0;
                bool res = double.TryParse(number_two, out two);
                if (plus_minus_button == true)
                {
                    two = two * (-1);
                }
                switch (number_of_operation)
                {
                    case 1:
                        result = one + two;
                        break;
                    case 2:
                        result = one - two;
                        break;
                    case 3:
                        result = one * two;
                        break;
                    case 4:
                        if (two != 0)
                        {
                            result = one / two;
                        }
                        break;
                    case 5:
                        result = Math.Pow(one, two);
                        break;
                    default:
                        break;
                }
                textBox.Text = result.ToString();
                one = result;
                number_of_operation = 1;
                first_number = false;
                number_two = "";
            }
            else
            {
                number_of_operation = 1;
                first_number = false;
                bool res = double.TryParse(number_one, out one);
                if (plus_minus_button == true)
                {
                    one = one * (-1);
                }
            }
            dot_button = false;
            plus_minus_button = false;
        }

        private void buttonMinus_Click(object sender, RoutedEventArgs e)
        {
            if (number_two.Equals("") == false)
            {
                double result = 0;
                bool res = double.TryParse(number_two, out two);
                if (plus_minus_button == true)
                {
                    two = two * (-1);
                }
                switch (number_of_operation)
                {
                    case 1:
                        result = one + two;
                        break;
                    case 2:
                        result = one - two;
                        break;
                    case 3:
                        result = one * two;
                        break;
                    case 4:
                        if (two != 0)
                        {
                            result = one / two;
                        }
                        break;
                    case 5:
                        result = Math.Pow(one, two);
                        break;
                    default:
                        break;
                }
                textBox.Text = result.ToString();
                one = result;
                number_of_operation = 2;
                first_number = false;
                number_two = "";
            }
            else
            {
                number_of_operation = 2;
                first_number = false;
                bool res = double.TryParse(number_one, out one);
                if (plus_minus_button == true)
                {
                    one = one * (-1);
                }
            }
            dot_button = false;
            plus_minus_button = false;
        }

        private void buttonMultiply_Click(object sender, RoutedEventArgs e)
        {
            if (number_two.Equals("") == false)
            {
                double result = 0;
                bool res = double.TryParse(number_two, out two);
                if (plus_minus_button == true)
                {
                    two = two * (-1);
                }
                switch (number_of_operation)
                {
                    case 1:
                        result = one + two;
                        break;
                    case 2:
                        result = one - two;
                        break;
                    case 3:
                        result = one * two;
                        break;
                    case 4:
                        if (two != 0)
                        {
                            result = one / two;
                        }
                        break;
                    case 5:
                        result = Math.Pow(one, two);
                        break;
                    default:
                        break;
                }
                textBox.Text = result.ToString();
                one = result;
                number_of_operation = 3;
                first_number = false;
                number_two = "";
            }
            else
            {
                number_of_operation = 3;
                first_number = false;
                bool res = double.TryParse(number_one, out one);
                if (plus_minus_button == true)
                {
                    one = one * (-1);
                }
            }
            dot_button = false;
            plus_minus_button = false;
        }

        private void buttonDivide_Click(object sender, RoutedEventArgs e)
        {
            if (number_two.Equals("") == false)
            {
                double result = 0;
                bool res = double.TryParse(number_two, out two);
                if (plus_minus_button == true)
                {
                    two = two * (-1);
                }
                switch (number_of_operation)
                {
                    case 1:
                        result = one + two;
                        break;
                    case 2:
                        result = one - two;
                        break;
                    case 3:
                        result = one * two;
                        break;
                    case 4:
                        if (two != 0)
                        {
                            result = one / two;
                        }
                        break;
                    case 5:
                        result = Math.Pow(one, two);
                        break;
                    default:
                        break;
                }
                textBox.Text = result.ToString();
                one = result;
                number_of_operation = 4;
                first_number = false;
                number_two = "";
            }
            else
            {
                number_of_operation = 4;
                first_number = false;
                bool res = double.TryParse(number_one, out one);
                if (plus_minus_button == true)
                {
                    one = one * (-1);
                }
            }
            dot_button = false;
            plus_minus_button = false;
        }

        private void buttonResult_Click(object sender, RoutedEventArgs e)
        {
            double result = 0;
            bool res = double.TryParse(number_two, out two);
            if (plus_minus_button == true)
            {
                two = two * (-1);
            }
            switch (number_of_operation)
            {
                case 1:
                    result = one + two;
                    break;
                case 2:
                    result = one - two;
                    break;
                case 3:
                    result = one * two;
                    break;
                case 4:
                    if (two != 0)
                    {
                        result = one / two;
                    }
                    break;
                case 5:
                    result = Math.Pow(one, two);
                    break;
                default:
                    break;
            }
            textBox.Text = result.ToString();
            one = result;
            number_one = one.ToString();
            first_number = false;
            number_two = "";
            dot_button = false;
            plus_minus_button = false;

        }

        private void buttonSqrt_Click(object sender, RoutedEventArgs e)
        {
            double result = 0;
            bool res = double.TryParse(number_one, out one);
            if (plus_minus_button == false)
            {
                result = Math.Sqrt(one);
            }

            textBox.Text = result.ToString();
            one = result;
            first_number = false;
            number_one = one.ToString();
            number_two = "";
            plus_minus_button = false;
            dot_button = false;
        }

        private void buttonPower_Click(object sender, RoutedEventArgs e)
        {
            if (number_two.Equals("") == false)
            {
                double result = 0;
                bool res = double.TryParse(number_two, out two);
                if (plus_minus_button == true)
                {
                    two = two * (-1);
                }
                switch (number_of_operation)
                {
                    case 1:
                        result = one + two;
                        break;
                    case 2:
                        result = one - two;
                        break;
                    case 3:
                        result = one * two;
                        break;
                    case 4:
                        if (two != 0)
                        {
                            result = one / two;
                        }
                        break;
                    case 5:
                        result = Math.Pow(one, two);
                        break;


                    default:
                        break;
                }
                textBox.Text = result.ToString();
                one = result;
                number_of_operation = 5;
                first_number = false;
                number_two = "";
            }
            else
            {
                number_of_operation = 5;
                first_number = false;
                bool res = double.TryParse(number_one, out one);
                if (plus_minus_button == true)
                {
                    one = one * (-1);
                }
            }
            dot_button = false;
            plus_minus_button = false;
        }
    }
}
